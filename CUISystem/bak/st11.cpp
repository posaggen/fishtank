#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <queue>
#include <ctime>
#include <set>
#include <map>
#include <string>
#include <bitset>

typedef long long LL;
#define pb push_back
#define MPII make_pair<int, int>
#define PII pair<int, int>
#define sz(x) (int)x.size()

using namespace std;

template<class T> T abs(T x){if (x < 0) return -x; else return x;}

#include "st11.h"

st11::st11(){
	setIdentifier("st11");
	srand((int)time(0));
	for (int i = 0; i < 100; ++i) level[i] = 0;
	for (int i = 0; i < 100; ++i) delta[i] = 1;
	Round = 0;
	Death = 0;
	Emergency = 0;
	AP = 0;
}

st11::~st11(){}

void st11::increaseDefence(){
	double hp = getMaxHP(), ar = getAr();
	double hp1 = (hp + 3.0) * (10.0 + ar) / 10.0, hp2 = hp * (ar + 11.0) / 10.0;
	if (hp1 * 1.02 > hp2 - eps) increaseHealth(); else{
		increaseArmor();
	}
}

Node st11::Dosomething(int x, int y){
	Node t(getHP(), getMaxHP());
	t.bonus = 0;
	int Dist = 100;
	for (int i = 1; i <= N; ++i)
		for (int j = 1; j <= M; ++j){
			if ((askWhat(i, j) == FOOD && abs(i - x) + abs(j - y) > 1)) Dist = min(Dist, abs(x - i) + abs(y - j));
		}
	t.x = x;
	t.y = y;
	t.Dist = Dist;
	for (int k = 0; k < 4; ++k){
		int u = x + dx[k], v = y + dy[k];
		if (u <= 0 || v <= 0 || u > N || v > M) continue;
		double bonus = 0;
		int f = askWhat(u, v);
		bool isalgae = false;
		if (f == FOOD) isalgae = true;
		if (f > 0 && f != getID()){
			double Ar = getAr() - 20;
			if (Round <= 10) Ar = max(Ar, 2.0); else 
				if (Round <= 50) Ar = max(Ar, 5.0); else 
					if (Round <= 100) Ar = max(Ar, 10.0); else Ar = max(Ar, 20.0);
			if ((double)askHP(f) < (double)getAtt() * (1.0 - Ar / (Ar + 10.0)))	bonus = level[f]; else bonus = level[f] / 10.0;
		}
		Node p(getHP(), getMaxHP());
		p.x = x;
		p.y = y;
		p.u = u;
		p.v = v;
		p.Dist = Dist;
		p.bonus = bonus;
		p.isalgae = isalgae;
		t = t + p;
	}
	return t;
}

Node st11::Update(int x, int y, int speed){
	Node Best = Dosomething(x, y);
	for (int i = 1; i <= N; ++i)
		for (int j = 1; j <= M; ++j){
			if (abs(x - i) + abs(y - j) > speed) continue;
			if (askWhat(i, j) != EMPTY) continue;
			Best = Best + Dosomething(i, j);
		}
	return Best;
}

void st11::init(){
	for (int i = 0; i < 1; ++i) increaseStrength();
	for (int i = 0; i < 0; ++i) increaseArmor();
	for (int i = 0; i < 4; ++i) increaseHealth();
	for (int i = 0; i < 5; ++i) increaseSpeed();
}

void st11::AddPoint(){
	while (Emergency && getPoint()){
		increaseHealth();
		--Emergency;
	}
	if (Round <= 15){
		while (getPoint()){
			if (AP % 9 == 1 || AP % 9 == 3 || AP % 9 == 5 || AP % 9 == 7) increaseDefence(); else 
				if (AP % 9 == 0) increaseStrength(); else increaseSpeed();
			++AP;
		}
	} else if (Round <= 200){
		while (getPoint()){
			if (AP % 9 == 1 || AP % 9 == 3 || AP % 9 == 5 || AP % 9 == 7) increaseDefence(); else 
				if (AP % 9 == 0) increaseSpeed(); else {
					if (rand() % 2) increaseSpeed(); else increaseStrength();
				}
			++AP;
		}
	} else {
		while (getPoint()){
			if (AP % 9 == 1 || AP % 9 == 3 || AP % 9 == 5 || AP % 9 == 7) increaseDefence(); else 
				if (AP % 9 == 0) increaseStrength(); else {
					if (rand() % 2) increaseSpeed(); else increaseStrength();
				}
			++AP;
		}
	}
}

void st11::play(){
	for (int i = 1; i <= getTotalPlayer(); ++i){
		level[i] += delta[i];
	}
	++Round;
	AddPoint();

	Node Best = Update(getX(), getY(), getSp());

	if (!Best.isalgae && Best.bonus < 1.5 && Best.Dist == 100){
		int tx, ty;
		double tb = -1;
		for (int i = 1; i <= N; ++i){
			for (int j = 1; j <= M; ++j)
				if (abs(i - getX()) + abs(j - getY()) < 4 * getSp()){
					int f = askWhat(i, j);
					if (f > 0 && f != getID()){
						double Ar = getAr() - 20, bonus;
						if (Round <= 10) Ar = max(Ar, 2.0); else 
							if (Round <= 50) Ar = max(Ar, 5.0); else 
								if (Round <= 100) Ar = max(Ar, 10.0); else Ar = max(Ar, 20.0);
						if ((double)askHP(f) < (double)getAtt() * (1.0 - Ar / (Ar + 10.0)))	bonus = level[f]; else bonus = level[f] / 10.0;
						if (tb < bonus){
							tx = i;
							ty = j;
							tb = bonus;
						}
					}
				}
		}
		int rest = getSp(), dx = getX(), dy = getY();
		if (abs(getX() - tx) <= rest){
			rest -= abs(getX() - tx);
			dx = tx;
		} else {
			rest = 0;
			if (getX() > tx) dx = getX() - rest; else dx = getX() + rest;
		}
		if (abs(getY() - ty) <= rest){
			rest -= abs(getY() - ty);
			dy = ty;
		} else {
			rest = 0;
			if (getY() > ty) dy = getY() - rest; else dy = getY() + rest;
		}
		Best.x = dx;
		Best.y = dy;
		Best.u = dx - 1;
		Best.v = dy;
	}

	move(Best.x, Best.y);
	int preEXP = getExp();
	int ff = askWhat(Best.u, Best.v);
	if (askWhat(Best.u, Best.v) != EMPTY) attack(Best.u, Best.v);
	if (preEXP < getExp()){
		delta[ff] += 0.15;
	} else delta[ff] += 0.2;
	AddPoint();

	for (int i = 0; i < 100; ++i){
		delta[i] -= 0.05;
		if (delta[i] < 0.08) delta[i] = 0.08;
	}
}


void st11::revive(int &x, int &y){
	Round += 2;
	Emergency += 4;
	++Death;
	Node Best;
	bool flag = false;
	for (int i = 1; i <= N; ++i){
		for (int j = 1; j <= M; ++j)
			if (askWhat(i, j) == EMPTY){
				Node t = Dosomething(i, j);
				if (!t.isalgae && Best.isalgae) continue;
				if (!Best.isalgae){
					Best = t;
				} else {
					int sum1 = 0, sum2 = 0, d1 = 100, d2 = 100;
					for (int u = 1; u <= N; ++u)
						for (int v = 1; v <= M; ++v)
							if (askWhat(u, v) > 0){
								if (abs(u - Best.x) + abs(v - Best.y) < getSp() - 3) ++sum1;
								if (abs(u - t.x) + abs(v - t.y) < getSp() - 3) ++sum2;
								d1 = min(d1, abs(u - Best.x) + abs(v - Best.y));
								d2 = min(d2, abs(u - t.x) + abs(v - t.y));
							}
					if (sum2 < sum1) Best = t; else 
						if (sum2 == sum1 && d2 > d1) Best = t;
				}
			}
	}
	x = Best.x;
	y = Best.y;
}
