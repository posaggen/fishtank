#include "env.h"
#include "TAAI.h"
#include "TAAI_HARD.h"
#include "TAAI_LUNATIC.h"
#include "TAAI_CRAZY.h"
#include "st11.h"

env& fish::host = env::GetInstance();

int main()
{
    env& system = env::GetInstance();
	system.addAI(new st11());
	system.addAI(new st11());
	system.addAI(new st11());
	system.addAI(new st11());
	system.addAI(new TAAI_HARD());
	system.addAI(new TAAI_HARD());
	system.addAI(new TAAI_HARD());
	system.addAI(new TAAI_HARD());
	system.addAI(new TAAI_HARD());
	system.addAI(new TAAI_LUNATIC());
	system.addAI(new TAAI_LUNATIC());
	system.addAI(new TAAI_LUNATIC());
	system.addAI(new TAAI_LUNATIC());
	system.addAI(new TAAI_LUNATIC());
    system.play();
}

