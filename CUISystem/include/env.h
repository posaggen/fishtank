#ifndef ENV_H
#define ENV_H

#include "constants.h"
#include <cstdlib>
#include <fstream>

class fish;

class env
{
public:
    ~env();
    bool addAI(fish*);
    void play();

private:
    void foodRefresh();
    bool sortCmp(int, int) const;
    void fishInit();
    void fishPlay();
    void calcPriority();
    void randXY(int&, int&);
    void printResult();
    int dis(int, int, int, int) const;
    int dis(int, int) const;
    bool validCor(int, int) const;

private:
    int current;
    fish* player[MAX_PLAYER+1];
    int fishNum;

    int map[N+1][M+1];
    typedef int fishData[MAX_PLAYER+1];
    fishData fishPoint;
    fishData fishLevel;
    fishData fishExp;
    fishData fishX;
    fishData fishY;
    fishData fishHP;
    fishData fishMaxHP;
    fishData fishAtt;
    fishData fishSp;
    fishData fishAr;
    fishData fishKill;
    fishData fishDie;
    fishData fishScore;
    fishData fishReviveCD;

    int phase;

    enum{INIT_PHASE, REVIVE_PHASE, MAIN_PHASE, MOVE_PHASE, ATTACK_PHASE};

    int sequence[MAX_PLAYER+1];

public:
    bool move(int, int);
    bool attack(int, int);
    int getPoint() const;
    int getLevel() const;
    int getExp() const;
    int getX() const;
    int getY() const;
    int getHP() const;
    int getMaxHP() const;
    int getAtt() const;
    int getSp() const;
    int getAr() const;
    int getID() const;
    int askWhat(int, int) const;
    int askHP(int) const;
    int getTotalPlayer() const;
    bool increaseHealth();
    bool increaseStrength();
    bool increaseSpeed();
    bool increaseArmor();

private:
    void increasePoint(int);
    void increaseLevel(int);
    void increaseExp(int);
    void increaseHP(int);
    void decreaseHP(int, int);
    void refreshScore(int);

public:
    static env& GetInstance();

private:
    env();
    env(const env&);
    env& operator=(const env&);
};

inline int env::dis(int x1, int y1, int x2, int y2) const
{
    return (std::abs(x1 - x2) + std::abs(y1 - y2));
}

inline int env::dis(int x, int y) const
{
    return dis(x, y, getX(), getY());
}

inline bool env::validCor(int x, int y) const
{
    return ((x >= 1) && (x <= N) && (y >= 1) && (y <= M));
}

#endif // ENV_H
