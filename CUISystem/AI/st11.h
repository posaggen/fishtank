#ifndef ST11_H
#define ST11_H

#include "fish.h"
#include "cstdio"
#include <cstdio>
#include <ctime>
#include <iostream>

const int dx[4] = {0, 0, 1, -1};
const int dy[4] = {1, -1, 0, 0};
const double eps = 1e-3;

class Node{
	
private:
	
public:
	int x, y, u, v, hp, mhp;
	double bonus;
	bool isalgae;
	int Dist;
	Node (int _hp = 0, int _mhp = 0):hp(_hp), mhp(_mhp){
		x = y = u = v = bonus = 0;
		isalgae = false;
		Dist = 100;
	}
	
	Node operator + (const Node &t)const{
		if (t.isalgae && isalgae){
			if (t.Dist < Dist) return t; else return *this;
		}
		if (t.isalgae){
			if ((double)hp < 0.5 * mhp) return t;
			if (bonus > 1.5) return *this; else return t;
		}
		if (isalgae){
			if ((double)hp < 0.5 * mhp) return *this;
			if (t.bonus > 1.5) return t; else return *this;
		}
				
		if (abs(t.bonus - bonus) < eps){
			if (t.Dist < Dist) return t; else return *this;
		}
		if (t.bonus > bonus) return t; else return *this;
	}
};

class st11:public fish{

public:
	
    int Round, AP, Death;
    int Emergency;
    FILE *output;
    
    double level[100];
    double delta[100];

    void AddPoint();

	void increaseDefence();

	Node Dosomething(int x, int y);

    Node Update(int x, int y, int speed);

    void init();
        
    void play();
    
    void revive(int &, int &);

    st11();
    
    ~st11();
};

#endif
